﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelController : MonoBehaviour
{
    private static PanelController _instance;
    public static PanelController Instance
    {
        get {
            if (_instance == null)
            {
                _instance =FindObjectOfType<PanelController>();
            }
            return _instance;
        }
    }
    public GameObject gameOverPanel;
    public GameObject winPanel;

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
