﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float smoothSpeed = 0.125f;
    [SerializeField] private Vector3 offset;

    private IDisposable cameraDisposable;
    private void Start()
    {
        cameraDisposable = Observable.EveryFixedUpdate().Subscribe(Follow);
    }

    private void Follow(long l)
    {
        Vector3 desiredPosition = target.position + offset;
        desiredPosition.x = transform.position.x;
        desiredPosition.y = transform.position.y;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
    }

    private void OnDestroy()
    {
        cameraDisposable.Dispose();
    }
}
