﻿using System;
using Lean.Touch;
using TMPro;
using UniRx;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float forwardSpeed;
    [SerializeField] private float sideSpeed;
    [SerializeField] private GameObject blockPrefab;
    [SerializeField] private TextMeshProUGUI scoreText;

    private ReactiveProperty<int> score;
    private Rigidbody rb;

    private IDisposable moveForwardDisposable;
    private IDisposable gameOverDisposable;

    void Start()
    {
        LeanTouch.OnFingerUpdate += OnFingerSwipe;
        score = new ReactiveProperty<int>();
        score.ObserveEveryValueChanged(property => property.Value).Subscribe(i => scoreText.text = i.ToString());
        moveForwardDisposable = Observable.EveryFixedUpdate().Subscribe(delegate
        {
            transform.Translate(Vector3.forward * forwardSpeed);
        });
        gameOverDisposable = Observable.EveryFixedUpdate().Select(l => transform.childCount).Where(i => i == 0)
            .Subscribe(i => EndGame(false));
    }

    public void EndGame(bool isGameWon)
    {
        gameOverDisposable.Dispose();
        LeanTouch.OnFingerUpdate -= OnFingerSwipe;
        Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(delegate
        {
            moveForwardDisposable.Dispose();
            if (isGameWon)
            {
                PanelController.Instance.winPanel.SetActive(true);
            }
            else
            {
                PanelController.Instance.gameOverPanel.SetActive(true);

            }
        });
    }

    private void OnFingerSwipe(LeanFinger finger)
    {
        Vector2 direction = finger.LastScreenPosition - finger.StartScreenPosition;
        MoveSideways(direction.normalized);
    }

    public void MoveSideways(Vector3 direction)
    {
        Vector3 newDirection = new Vector3(direction.x, 0, 0);
        transform.Translate(newDirection * sideSpeed);
        ClampPosition();
    }

    private void ClampPosition()
    {
        var position = transform.position;
        if (position.x > 3f)
        {
            position.x = 3f;
        }
        else if (position.x < -3f)
        {
            position.x = -3f;
        }

        transform.position = position;
    }

    public void AddBlock()
    {
        var allBlocks = GetComponentsInChildren<PlayerBlock>();
        foreach (var block in allBlocks)
        {
            block.transform.position += new Vector3(0, 1.2f, 0);
        }

        var position = transform.position;
        Vector3 firstBlockPosition = new Vector3(position.x, 1, position.z);
        Instantiate(blockPrefab, firstBlockPosition, Quaternion.identity, transform);
    }

    public void AddScore()
    {
        score.Value += 10;
    }
}