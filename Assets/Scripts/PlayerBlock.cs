﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class PlayerBlock : MonoBehaviour
{
    private GameObject deadBlocksParent;
    private IDisposable lavaDisposable;
    private void OnEnable()
    {
        deadBlocksParent = FindObjectOfType<BlockPool>().gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Block"))
        {
            other.gameObject.SetActive(false);
            transform.GetComponentInParent<PlayerController>().AddBlock();
        }

        if (other.gameObject.CompareTag("Diamond"))
        {
            other.gameObject.SetActive(false);
            transform.GetComponentInParent<PlayerController>().AddScore();
        }

        if (other.gameObject.CompareTag("Lava"))
        {
            lavaDisposable = Observable.Timer(TimeSpan.FromSeconds(0.1f)).Subscribe(l => DestroyBlockOnLava());
        }
        if (other.gameObject.CompareTag("FinishLine"))
        {
            transform.GetComponentInParent<PlayerController>().EndGame(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Lava"))
        {
            lavaDisposable.Dispose();
        }
    }

    private void DestroyBlockOnLava()
    {
        gameObject.SetActive(false);
        transform.parent = deadBlocksParent.transform;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            transform.parent = deadBlocksParent.transform;
        }
    }
}